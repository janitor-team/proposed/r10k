require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new do |spec|
  spec.exclude_pattern = [
    './spec/unit/forge/module_release_spec.rb',
  ].join ','
end

task :default => :spec
